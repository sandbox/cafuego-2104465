This module provides a few custom views handlers for functionality you would usually
embed in a view using the PHP module and custom PHP.

* Status by date
  Show a configurable status label depending on the value of a date field. This
  allows you to for instance list events as Open or Closed in a view.

* Multiple fields
  This allows you to show a single label on a multiple-value field. The field
  value separator is configurable.

* Search suggestions
  A views area plugin (header/footer) that can show search suggestions when
  a search_api_db search view results no results. The algorithm used to suggest
  search terms can be configured.


INSTALLATION
------------

Place the module directory under sites/all/modules and enable Cafuego Views
Widgets via the module administration page.

The new fields and areas are available under the 'Custom' label in Views.
