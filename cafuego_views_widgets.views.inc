<?php
/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function cafuego_views_widgets_views_data() {
  $data['views']['cafuego_views_widgets_multiple_field'] = array(
    'title' => t('Multiple Fields'),
    'group' => t('Custom'),
    'help' => t('Process multiple field values into one.'),
    'field' => array(
      'help' => t('Process multiple field values into one.'),
      'handler' => 'cafuego_views_widgets_handler_multiple_field',
    ),
  );

  $data['views']['cafuego_views_widgets_search_suggestions_area'] = array(
    'title' => t('Search suggestions'),
    'group' => t('Custom'),
    'help' => t('Provide search suggestions for the area.'),
    'area' => array(
      'help' => t('Provide search suggestions for the area.'),
      'handler' => 'cafuego_views_widgets_handler_search_suggestions_area',
    ),
  );

  $data['views']['cafuego_views_widgets_date_status_field'] = array(
    'title' => t('Status by Date'),
    'group' => t('Custom'),
    'help' => t('Provide open of closed status through a date field value.'),
    'field' => array(
      'help' => t('Provide open of closed status through a date field value.'),
      'handler' => 'cafuego_views_widgets_handler_date_status_field',
    ),
  );

  return $data;
}
