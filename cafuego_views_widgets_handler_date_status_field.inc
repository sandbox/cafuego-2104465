<?php

/**
 * A handler to provide a field that shows `Open' or `Closed' depending on
 * a date field value.
 *
 * @ingroup views_field_handlers
 */
class cafuego_views_widgets_handler_date_status_field extends views_handler_field {

  /**
   * Implements views_object#option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['status_date_field'] = array('default' => FALSE);
    $options['status_date_open'] = array('default' => t('Open'));
    $options['status_date_closed'] = array('default' => t('Closed'));
    return $options;
  }

  /**
   * Implements views_handler#options_form().
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      // Not self!
      if ($field == $this->field) {
        continue;
      }

      // Only date fields.
      if ($handler->field_info['module'] != 'date') {
        continue;
      }

      $title = (empty($handler->options['ui_name']))
        ? t('@group: @title', array('@group' => $handler->definition['group'], '@title' => $handler->definition['title']))
        : $handler->options['ui_name'];
      $fields[$handler->handler_type . '_' . $field] = check_plain($title);
    }

    $form['status_date_field'] = array(
      '#type' => 'select',
      '#title' => t('Source field'),
      '#description' => t('Choose the source date field to use.'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->options['status_date_field'],
    );

    $form['status_date_open'] = array(
      '#type' => 'textfield',
      '#title' => t('Open value'),
      '#description' => t('The value to display when the current date falls within the specified field date range.'),
      '#default_value' => $this->options['status_date_open'],
    );

    $form['status_date_closed'] = array(
      '#type' => 'textfield',
      '#title' => t('Closed value'),
      '#description' => t('The value to display when the current date falls outside the specified field date range.'),
      '#default_value' => $this->options['status_date_closed'],
    );
  }

  /**
   * Implements views_handler_field#query().
   *
   * @see views_php_views_pre_execute()
   */
  function query() {
    // Provide a field alias but don't actually alter the query.
    // $this->field_alias = 'views_multiple_field_' . $this->position;
  }

  /**
   * Implements views_handler_field#render().
   */
  function render($values) {
    $value = NULL;
    $items = array();

    if (!empty($this->options['status_date_field'])) {
      if (isset($values->{$this->options['status_date_field']}[0])) {
        $dates = $values->{$this->options['status_date_field']}[0]['raw'];

        // Convert the dates and check if time() is between them.
        $start = strtotime($dates['value']);
        $end   = strtotime($dates['value2']);
        $now   = time();

        // If there are no values, we're open right?
        if (empty($start) || empty($end)) {
          return check_plain($this->options['status_date_open']);
        }

        // Add a day to the closing date, so the closing time is midnight at the
        // end of the chosen day, rather than at the start.
        $end += (60 * 60 * 24);

        // If there are dates and now is in the correct range, we're open and
        // otherwise closed. If no dates, return empty.
        if ($start <= $now && $now <= $end) {
          return check_plain($this->options['status_date_open']);
        }
        else {
          return check_plain($this->options['status_date_closed']);
        }
      }
    }
    return check_plain($this->options['status_date_open']);
  }
}
