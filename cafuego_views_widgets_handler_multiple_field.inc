<?php

/**
 * A handler to provide a field that can display a single field label for a
 * field with multiple values.
 *
 * @ingroup views_field_handlers
 */
class cafuego_views_widgets_handler_multiple_field extends views_handler_field {

  /**
   * Implements views_object#option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['field'] = array('default' => FALSE);
    $options['separator'] = array('default' => ', ');
    $options['natural'] = array('default' => FALSE);
    $options['oxford'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Implements views_handler#options_form().
   */
  function options_form(&$form, &$form_state) {
    if (!empty($this->options['oxford'])) {
      drupal_set_message(t('Using an Oxford comma is wrong.'), 'error');
    }

    parent::options_form($form, $form_state);

    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      // Not self!
      if ($field == $this->field) {
        continue;
      }
      $title = (empty($handler->options['ui_name']))
        ? t('@group: @title', array('@group' => $handler->definition['group'], '@title' => $handler->definition['title']))
        : $handler->options['ui_name'];
      $fields[$handler->handler_type . '_' . $field] = check_plain($title);
    }

    $form['field'] = array(
      '#type' => 'select',
      '#title' => t('Source field'),
      '#description' => t('Choose the source tags field to use.'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->options['field']
    );

    $form['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#description' => t('Choose the list separator to use.'),
      '#default_value' => empty($this->options['separator']) ? ', ' : $this->options['separator'],
    );

    $form['natural'] = array(
      '#type' => 'checkbox',
      '#title' => t('Natural list'),
      '#description' => t('Use "<em>and</em>" instead of the specified separator to separate the last item on the list. For example: <em>yellow, red, green and blue</em>.'),
      '#default_value' => $this->options['natural']
    );

    $form['oxford'] = array(
      '#type' => 'checkbox',
      '#title' => t('Oxford comma'),
      '#description' => t('Use an Oxford comma in the natural list. For example: <em>yellow, red, green, and blue</em>.'),
      '#default_value' => $this->options['oxford'],
      '#disabled' => TRUE,
    );
  }

  /**
   * Implements views_handler_field#query().
   *
   * @see views_php_views_pre_execute()
   */
  function query() {
    // Provide a field alias but don't actually alter the query.
    $this->field_alias = 'cafuego_views_widgets_multiple_field_' . $this->position;
  }

  /**
   * Implements views_handler_field#render().
   */
  function render($values) {
    $value = NULL;
    $items = array();

    if (!empty($this->options['field'])) {
      foreach ($values->{$this->options['field']} as $item) {
        $items[] = check_plain($item['rendered']['#markup']);
      }
    }

    // Check if we want a natural list and an Oxford comma.
    $last = '';
    if ($this->options['natural']) {
      if (count($items) > 1) {
        // No Oxford comma unless there are at least three items.
        if (count($items) > 2 && $this->options['oxford']) {
          $last = $this->options['separator'];
        }
        // Items are already safe; this allows users to simply translate 'and' if needed.
        $last .= t('and') . ' ' . array_pop($items)));
      }
    }

    return implode($this->options['separator'], $items) . $last;
  }
}
